FROM openjdk:8-jdk as builder
COPY . .
RUN chmod +x mvnw
RUN ./mvnw package -DskipTests


FROM openjdk:8-jdk-alpine
COPY --from=builder /target/devOpsDemo-0.0.1-SNAPSHOT.jar ./app.jar
EXPOSE 2222

CMD ["java", "-jar", "app.jar"]
